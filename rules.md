# Mixologist

## Introduction

You have just started your new career in The Pink Cucumber, the most famous cocktail bar in town.  You've been hired as a trainee along with other trainees by Inga the Head Mixologist, running the bar has made her rich and she's getting ready to retire to a tropical island. Inga's looking for a successor to take over and you think you've got what it takes to be that successor, the trouble is so do the other new trainees.  The wages are low but there is money to be made from tips and if you can prove yourself the Head Mixologist is the most distinguished job in the region.  Plus you can always try to earn a bit extra through a cheeky wager now and again.

As a trainee you're new to shaking cocktails, in fact you're starting off on serving shots so you can get a feel of life behind the bar.  If you want to progress and become Head Mixologist you're going to need to learn some new skills.  Inga's willing to teach you but you're going to have to pay for her time which is always in high demand from students.  There are other ways of learning such as trial and error but if you get caught upsetting the customers you'll be sent to wash the pots.

### Game Overview

* You earn experience points by serving customers drinks
* Customers who enjoy their drinks more will tip you some money
* You use money to develop your skills
* The more skills you have the more different types of glasses you'll unlock and the more complex the drinks you can make
* The more complex the drinks you make the greater the experience points and tips
* Throughout the game you can unlock new capabilities, gamble to make (or loose) money, and take a risk on mixing drinks you're not yet skilled to mix
* You'll need to find the right customers and sit them to a table where they'll wait to be served
* There is more than one way to serve a customer and the way in which you do it will be easier or harder and can return bigger or smaller rewards
* Beware if you don't serve the quick enough then an opponent might serve them instead
* You'll need to take care of your glasses and wash them after each use
* The player with the most experience points at the end will be the Head Mixologist

### Contents

* Bar board
* Player board (shape of a tray)
* Customer Cards
* Clear disc (glass tokens)
* Player skills counters
* Player Chance counters
* Ingredients Dice x 6
* Money


### Playing the game - a quick overview

* The aim of the game is to serve drinks to customers and gain experience points for doing so
* Before a player can serve a customer they must have 3 things in place:  
  1. some reserved ingredients (see ingredients)
  2. a customer waiting to be served (see customers)
  3. a clean glass to use (see glasses)
* Ingredients are shown on the bar board and players must place a skill cube or risk token onto an ingredient to reserve it, an ingredient can be reserved by only one player at a time
* Players select customer cards from a draw pile but may only hold up to 5 customers at any one time, this is the player's customer hand
* Customer's the player is holding hand can not be served whilst in their hand, they must be placed a spare table on their player board to serve them
* A player may serve a customer at a table on their player board  or serve a customer who is waiting to be served on another player's board
* To serve a customer the player must mix the drink that the customer is requesting using the ingredients that they have reserved
* A drink that a customer is requesting is described on the customer card through its taste rather than specific ingredients, ie. a "slippery electric eel" is a little bit fishy, is fiery with a dash of saltiness
* there are many ingredients to choose from, the ingredients have one or more tastes associated to them (such as fishiness and saltiness for the anchovies and fairy flavours for the tequila)
* You can only serve a customer by matching the number of different tastes they want to the different tastes associated to the ingredients
* Each ingredient has a skill level associated to it and for each skill cube placed to make the drink you are allowed a single die to contribute to the success of making the drink
* Making the drink means shaking all the dice available to the player for that drink in the cocktail shaker
* If the sum of the dice is equal to or greater than the sum of the skill required across all ingredients then the drink has successfully been made

* If the drink is successfully served:
  * the player receives experience points shown on the card
  * Bonuses are received such as tips for using the correct glass or adding the correct accessory (ice, umbrella, etc)
  * any skill cubes and risk tokens reserving ingredients are  returned to the player
  * the customer card is placed in a completed customer pile for that player (unless otherwise stated on the card)
  * the glass that has been used is placed in the washing up area

* If the dice roll is _not_ successful then:
  * the customer has not been served and the experience or bonuses are not received
  * any skill cubes and risk tokens reserving ingredients are  returned to the player
  * the customer remains in front of the player at their table waiting to be served
  * if the drink was not made and a risk token was used then the glass is broken and is sent to the washing up area

* There are other options as turns for the player to take throughout the game which can increase chances of being successful, increase skills and abilities, increase money etc.  These are explained further in the game rules.

* The game continues until there are no more customers left in the draw pile
  * experience points are tallied (along the board edge)
  * bonus experience points are added to the track
  * any customers sat at tables at the end of the game count as negative experience points for that player
  * The player with the most experience at the end of the game is the Head Mixologist

## Game Rules

### Set up

* The bar board is set up in the middle of the table
* The deck of customer cards are shuffled and placed face down where all players can reach them
* Money is also placed where each player can reach it
* Player experience markers are places on the zero (0) of the experience track

* Each player receives a player board and:
  * Places a skill cube on each inactive cube marker against the glass markers on their board
  * Adds an extra skill cube to the active skill cube area on the player board
  * Places their shot glass counter over the shot glass marker
  * All their other glass counters are placed next to their player board
  * The two player risk counters are placed over the risk markers on their board
* The dice and cocktail shaker is placed within reach of each player

### Objective of the game

Serve drinks to customers in return for experience points, the player with the most experience points at the end of the game is the Head Mixologist.

### Phases and turns

The game starts with the person who last made a cocktail
Each player takes it in turn to do just one of the following ACTIONS:

* select a customer card
* reserve a cocktail ingredient
* serve a customer
* learn a new skill
* place a wager
* wash your pots (take a glass)

The game ends when there are no more player cards in the draw pile (see questions)

### ACTION: Selecting a customer card

* Throughout the game players will need to draw customer cards to add to their hand
* Customer cards are drawn from the face down deck
* A player may only hold up to 5 customer cards in their hand
* If a player draws a 6th customer card then the player must discard a customer

### ACTION: Reserving a cocktail ingredient

* A player has a number active skill cubes (1-6) and risk counters (2)
* A skill cube or risk counter can be placed on an ingredient at the bar
* An ingredient can only be reserved by a single player
* The skill cubes placed on ingredients represent the dice that will be used during the making of a drink
* An ingredient can have more than one skill cube from the player

#### types of Ingredients

* There are lots of ingredients to choose from at the bar 
* Each ingredient has tastes and a skill number associated to it
* The tastes are used to match the ingredients to the customer drink requests
* The skill number is used when mixing the drink and will affect on how successful the player is at making the drink (lower numbers are easier)

The ingredients that can be reserved are as follows:
* spirits/Wine
  * Vodka fiery 2, dry 2 (skill 3)
  * Tequila fiery 4 (skill 4)
  * Whisky fiery 3 (skill 3)
  * Gin dry 3 (skill 2)
  * rum spicy 2 fiery 1 (skill 2)
  * etc...
* Mixers/Flavourings
  * cola sweet 3 (skill 2)
  * rose flower 4 (skill 4)
  ...
* Fruit/Herbs
  * lemon sour/citrus 2
  * salt salty 3
  * mint minty 3
  ...
* Accessories
  * not technically an ingredient although an accessory can be reserved and added to a drink
  * These are items are often needed to gain bonuses when serving a customer
  * items include ice cubes, crushed ice, umbrella, cherry, olive, plastic monkey, sparkler, etc

#### Ingredients' tastes

* Ingredients have associated tastes such as 
  * fiery
  * spicy
  * sweet
  * dry
  * fruity
  * flowery
  * salty
  * sour
  * (could add a few obscure ones?  Fishy?  Eggy?)
* These are shown against an ingredient in the bar area
* they are also shown on a customer card specifying the taste of a requested drink

### ACTION: Serving a Customer

* Customer cards show several things:

1. The name of the drink the customer wants
2. The tastes of the drink
3. Experience points awarded for serving the drink
4. A preferred glass the customer would like along with a tip to be given if that glass is used
5. Other bonus actions for other items served with the drink that the customer will appreciate

#### Prerequisites of serving a customer

* The player must have reserved the right ingredients
  * the ingredients reserved must be able to match the taste preferences on the customer card
  * eg. if a customer wants a drink that has 3 fire and 2 sugar then the player must have reserved ingredients that make up to 3 fire and 2 sugar
  * note: there is often more then one and sometimes many ways of having the right ingredients to serve the customer
* The customer will need to be seated 
  * A customer may already be seated on a player's board
  * if the customer is coming from the player's hand they will need to be seated at an empty seat on the player's board
	* if there is no empty seat then the customer can not be served
* The player must also have a clean glass on their board to be used to serve the drink
  * if the player does not have a clean glass they must retrieve one from the washing up area first (see wash pots)

#### Making a drink

* If all prerequisites are in place then the player can attempt to make the drink
* To make a drink dice are shaken (nor stirred) in the cocktail shaker and rolled to see if the drink was successfully made or not
* Each reserved ingredient has a skill number associated to it
* The player adds a die per skill cube to the cocktail shaker
  * note: dice are not added for risk tokens
* If the sum of the dice rolled is equal to or greater than the sum of the skill numbers of the ingredients being used then the drink was successfully made

#### If the drink was successfully made

* Then the customer was successfully served and points, tips and bonuses are awarded:
  * The experience points shown on the card are added onto the players experience points on the track on the bar board
  * If the glass used matches the glass on the card then the player receives a tip shown against the glass and takes that amount of money from the pile on the table
  * The card may also show other bonuses/actions that may need to be used immediately or can be saved for another turn
	* eg. a card may show that the customer likes pink flamingos and if the player added one to the drink as an accessory there may be a bonus such as a one off addition of a chilli to a drink resulting in 3 fire without need to reserve or shake for an ingredient.
* The customer is removed from the seat and placed in the player's served customers pile
  * if there is an action to be used at a later time the card can be put to one side, advised to be tucked under the player's  board with the action/bonus showing as a reminder
* The skill cubes and any risk counters used to reserve the ingredients are returned to the player
* The glass that was used will need washing and is sent to the washing up area

#### If the drink was _not_ successfully made

* Then the customer was not served and will remain seated at the table
  * Any player in the game is now free to serve this customer if they can and want to
* Any skill cubes and risk counters used to reserve the ingredients are returned to the player
* If risk counters were used then the glass is sent to the washing up area as a forfeit 

### ACTION: Learn a new skill

Learning new skills will help you create more complex drinks, be more successful in your mixing and allow you to serve a wider variety of glasses, increasing your chances of tips.   But it costs in both money and time, and the higher you go the more it will cost (but the more it will pay off too).

* Each time a player learns a new skill they improve their ability in 2 ways
  1. They receive a new skill cube to use to reserve ingredients
  2. They unlock a glass of their choice

* A player must pay to learn a new skill depending on the number of skill cubes already owned
  * For each cube a player must pay 3GP into the bank
* A player can then place all their cubes into the training area
* An extra skill cube can then be added from the inactive skill cubes on the player's board
  * Each inactive skill cube on the player's board is associated to a glass
  * Choosing an inactive skill cube to add to the training area also unlocks that glass
  * A glass token can now be added against/over the unlocked glass marker on the board
* For subsequent turns the player can retrieve the skill cubes from the training area and add them into their active cube zone on their player board.

#### Glasses

* When the right glass is used to make a drink for a customer the customer generally gives the player a tip
* Each player board shows glass markers depicting the glasses that can be used in the game
* When starting the game the only glass available to each player is the shot glass
* Each glass has a clear glass disc than can be placed over the glass on the player board, this signifies that the player has a glass available
* Glasses can be taken out of the pot washing area but may only be placed on a glass that has been unlocked (like the shot glass)
* Each player starts the game with one glass token on the shot glass
* Inactive skill tokens lock the glasses from play, only when a new skill is gained can a new glass be unlocked and made available to the player
* Any glass can be chosen to be unlocked when gaining a new skill 
* when unlocking a new glass the player receives an extra clear glass token to place over the respective glass marker on the player board

* The different glasses are:
	* Shot
	* Highball
	* Low-ball
	* flute
	* martini
	* tropical

###  ACTION: Make a wager

* The Pink Cucumber isn't all about making drinks, there are mini games to be held in the bar too.
* The mini games allow players to place bets to increase their money and fast track to afford to learn a new skill

* Take a chance by placing a bet and shaking the dice
* All players can take part whenever one player decides to do it
* Different options different odds, player who's turn it is decides which one
* Player who's turn it is decides how much to enter, anyone else has to match it
* All money goes into a pot
* Winners share total return on the pot

(to test: we should try having these 3 buckets or having a single option to bet and roll with 6, 7, 8 being loose, 5 or 9 = evens, 4 or 10 = 2:1, 3 or 11 = 3:1, 2 or 12 = 5:1)

### ACTION: Washing the pots

* When a glass is used it is sent to the wash pots area
* the player must take a turn to wash the glass and return it to an unlocked and empty glass marker on the player board.

## Ending the Game

* The game ends when ??? (the customer cards run out?, when the next customer is served after the last customer card is taken from the draw pile)
* Any end game bonus experience points get added on
* Any players with customers still at their tables get negative exp for those customers
* The player with the most exp points is promoted to Head Mixologist




# Questions and things to try out

* Gambling mechanics - different pots with different odds or single pot with different odds depending on dice outcome
* Turns, just continue in turns with all options available or do in phases: (prepare to serve, serve, wash up, etc)
* Washing up - should each glass take a turn to return or would it be better to have to pay to retrieve the glass during a wash up phase
* Should different player board have different capabilities such as different luck when gambling or different forfeits when taking a risk?
* how should the game end?  If it ends as currently suggested then there is no value in anyone picking up the final customer card... - how about the game ends when the next customer is served after the final customer card in the draw pile has been taken... Could there be other options to signify end of game beyond simply reaching an experience goal?
  * an alternative to players holding customer cards could be that customers are shown to tables (5 slots?) on the bar board and as they get served they are replaced by new customers though I like the idea of a player working up to serving a customer and if they don't succeed then other players could "steal" them from the player
* what other forfeit could be applied when using risk tokens and failing to successfully make the drink
* could there be missions to save until the end like player who served the most fishy drinks?
* could different players have different special abilities (one player is more lucky, one player doesn't have to wash pots, etc)
* could there be drink cards (tastes + exp points) and customer cards (glasses, accessories + tips and bonuses) that get paired up to make a waiting customer?
