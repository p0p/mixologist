# Mixologist

Mixologist is a Euro style board game where players compete to serve drinks to customers, gain experience points and become Head Mixologist.


An open source board game under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
